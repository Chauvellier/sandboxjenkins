## Description

Ce projet permet de créer une VM CentOS 7 avec:
* Docker CE
* docker-compose
* Jenkins-LTS (alpine)


## pré-requis 

Ce projet est a exécuter sur un environnement Windows.
La vitualisation windows doit être activée.

Les software suivant installés:
* [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
* [Vagrant](https://www.vagrantup.com/)

## 1er lancement

Pour le 1er lancement, il est nécessaire d'effectuer la commande suivante:
```sh
> vagrant up
```

suite à cette commande, la box vagrant CentOS est téléchargé, la VM créée et les scripts Ansible exécutés.
Si tout est OK, la page web http://127.0.0.1:9000 affichera une page d'initialisation de Jenkins.

## Initialisation de Jenkins

Pour initialiser Jenkins un password est requis.
Afin de le récupérer, il faut se connecter sur la VM via la commande:
```sh
> vagrant ssh
```

Puis récupérer le password via la commande:
```sh
> sudo docker container exec jenkins cat /var/jenkins_home/secrets/initialAdminPassword
```
Suivez les différentes étapes par défaut dans l'initialisation de jenkins.

## Commandes à connaitre

Voici différentes commandes utiles pour gérer la VM / le container jenkins:

* Arret de la VM
```sh
> vagrant halt
```
* démarrer de la VM
```sh
> vagrant up
```
* Se conencter à la VM
```sh
> vagrant ssh
```
* Supprimer la VM
```sh
> vagrant destroy
```
* Vérifier l'état des container (sur la VM)
```sh
> sudo docker container ls
```
* Arrêter le container (sur la VM)
```sh
> sudo docker container stop jenkins
```
* Démarrer le container (sur la VM)
```sh
> sudo docker container start jenkins
```
* Vérifier les logs de jenkins (sur la VM)
```sh
> sudo docker container logs -f jenkins
```